package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        SubstringFilterSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : SubstringFilter
 */
public final class SubstringFilterSpecValidator {


    private SubstringFilterSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_attr_substr
     * The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview
     */
    private static boolean _validateSubstringFilterSpec_Constraint_ch_cpi_attr_substr(net.ihe.gazelle.hpd.SubstringFilter aClass) {
        return (!(((String) aClass.getName()) == null) && (aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.2", aClass.getName().toLowerCase()) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.3", aClass.getName().toLowerCase())));

    }

    /**
     * Validation of class-constraint : SubstringFilterSpec
     * Verify if an element of type SubstringFilterSpec can be validated by SubstringFilterSpec
     */
    public static boolean _isSubstringFilterSpec(net.ihe.gazelle.hpd.SubstringFilter aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   SubstringFilterSpec
     * class ::  net.ihe.gazelle.hpd.SubstringFilter
     */
    public static void _validateSubstringFilterSpec(net.ihe.gazelle.hpd.SubstringFilter aClass, String location, List<Notification> diagnostic) {
        if (_isSubstringFilterSpec(aClass)) {
            executeCons_SubstringFilterSpec_Constraint_ch_cpi_attr_substr(aClass, location, diagnostic);
        }
    }

    private static void executeCons_SubstringFilterSpec_Constraint_ch_cpi_attr_substr(net.ihe.gazelle.hpd.SubstringFilter aClass,
                                                                                      String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateSubstringFilterSpec_Constraint_ch_cpi_attr_substr(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_attr_substr");
        notif.setDescription("The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-SubstringFilterSpec-constraint_ch_cpi_attr_substr");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-003"));
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-005"));
        diagnostic.add(notif);
    }

}
