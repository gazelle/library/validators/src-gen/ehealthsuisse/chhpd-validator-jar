package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        AttributeValueAssertionSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : AttributeValueAssertion
 */
public final class AttributeValueAssertionSpecValidator {


    private AttributeValueAssertionSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_attr_valassert
     * The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview
     */
    private static boolean _validateAttributeValueAssertionSpec_Constraint_ch_cpi_attr_valassert(net.ihe.gazelle.hpd.AttributeValueAssertion aClass) {
        return (!(((String) aClass.getName()) == null) && (aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.2", aClass.getName().toLowerCase()) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.3", aClass.getName().toLowerCase())));

    }

    /**
     * Validation of class-constraint : AttributeValueAssertionSpec
     * Verify if an element of type AttributeValueAssertionSpec can be validated by AttributeValueAssertionSpec
     */
    public static boolean _isAttributeValueAssertionSpec(net.ihe.gazelle.hpd.AttributeValueAssertion aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   AttributeValueAssertionSpec
     * class ::  net.ihe.gazelle.hpd.AttributeValueAssertion
     */
    public static void _validateAttributeValueAssertionSpec(net.ihe.gazelle.hpd.AttributeValueAssertion aClass, String location, List<Notification> diagnostic) {
        if (_isAttributeValueAssertionSpec(aClass)) {
            executeCons_AttributeValueAssertionSpec_Constraint_ch_cpi_attr_valassert(aClass, location, diagnostic);
        }
    }

    private static void executeCons_AttributeValueAssertionSpec_Constraint_ch_cpi_attr_valassert(net.ihe.gazelle.hpd.AttributeValueAssertion aClass,
                                                                                                 String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateAttributeValueAssertionSpec_Constraint_ch_cpi_attr_valassert(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_attr_valassert");
        notif.setDescription("The defined attributes for the CHCommunity and CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-AttributeValueAssertionSpec-constraint_ch_cpi_attr_valassert");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-003"));
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-005"));
        diagnostic.add(notif);
    }

}
