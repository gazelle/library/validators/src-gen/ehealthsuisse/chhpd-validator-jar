package net.ihe.gazelle.hpd.validator.chhpdaddrequest;

import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHaddRequestForRelationshipTemplate
 * package :   chhpdAddRequest
 * Template Class
 * Template identifier :
 * Class of test : AddRequest
 */
public final class CHaddRequestForRelationshipTemplateValidator {


    private CHaddRequestForRelationshipTemplateValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_hpd_012_owner
     * When the addRequest describes a relationship, the owner attribute is required
     */
    private static boolean _validateCHaddRequestForRelationshipTemplate_Ch_hpd_012_owner(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result1;
        result1 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("owner")) {
                    result1.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > Integer.valueOf(0));


    }

    /**
     * Validation of template-constraint by a constraint : CHaddRequestForRelationshipTemplate
     * Verify if an element can be token as a Template of type CHaddRequestForRelationshipTemplate
     */
    private static boolean _isCHaddRequestForRelationshipTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=relationship){1}.*");

    }

    /**
     * Validation of class-constraint : CHaddRequestForRelationshipTemplate
     * Verify if an element of type CHaddRequestForRelationshipTemplate can be validated by CHaddRequestForRelationshipTemplate
     */
    public static boolean _isCHaddRequestForRelationshipTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return _isCHaddRequestForRelationshipTemplateTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHaddRequestForRelationshipTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     */
    public static void _validateCHaddRequestForRelationshipTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
        if (_isCHaddRequestForRelationshipTemplate(aClass)) {
            executeCons_CHaddRequestForRelationshipTemplate_Ch_hpd_012_owner(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHaddRequestForRelationshipTemplate_Ch_hpd_012_owner(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                         String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForRelationshipTemplate_Ch_hpd_012_owner(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_012_owner");
        notif.setDescription("When the addRequest describes a relationship, the owner attribute is required");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForRelationshipTemplate-ch_hpd_012_owner");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-012"));
        diagnostic.add(notif);
    }

}
