package net.ihe.gazelle.hpd.validator.chhpdaddrequest;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


public class CHHPDADDREQUESTPackValidator implements ConstraintValidatorModule {


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     */
    public CHHPDADDREQUESTPackValidator() {
    }


    /**
     * Validation of instance of an object
     */
    public void validate(Object obj, String location, List<Notification> diagnostic) {

        if (obj instanceof net.ihe.gazelle.hpd.AddRequest) {
            net.ihe.gazelle.hpd.AddRequest aClass = (net.ihe.gazelle.hpd.AddRequest) obj;
            CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateValidator._validateCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate(aClass, location, diagnostic);
            CHaddRequestForHCProfessionalTemplateValidator._validateCHaddRequestForHCProfessionalTemplate(aClass, location, diagnostic);
            CHaddRequestForHCRegulatedOrganizationTemplateValidator._validateCHaddRequestForHCRegulatedOrganizationTemplate(aClass, location, diagnostic);
            CHaddRequestForRelationshipTemplateValidator._validateCHaddRequestForRelationshipTemplate(aClass, location, diagnostic);
        }

    }

}

