package net.ihe.gazelle.hpd.validator.chhpdaddrequest;

import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHaddRequestForHCRegulatedOrganizationTemplate
 * package :   chhpdAddRequest
 * Template Class
 * Template identifier :
 * Class of test : AddRequest
 */
public final class CHaddRequestForHCRegulatedOrganizationTemplateValidator {


    private CHaddRequestForHCRegulatedOrganizationTemplateValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_hpd_004_Type
     * For  HPD Organizational provider  the parameter  Org Type  (businessCategory) is required
     */
    private static boolean _validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_004_Type(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result1;
        result1 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("businesscategory")) {
                    result1.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > Integer.valueOf(0));


    }

    /**
     * Validation of instance by a constraint : ch_hpd_013_IdentifierSyntax
     * At least one value for hcidentifier must start with  RefData:OID: .
     */
    private static boolean _validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_013_IdentifierSyntax(net.ihe.gazelle.hpd.AddRequest aClass) {
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                java.util.ArrayList<String> result2;
                result2 = new java.util.ArrayList<String>();

                /* Iterator Select: Select all elements which fulfill the condition. */
                try {
                    for (String anElement2 : anElement1.getValue()) {
                        if (aClass.matches(anElement2.toLowerCase(), "^refdata:oid:.*")) {
                            result2.add(anElement2);
                        }
                        // no else
                    }
                } catch (Exception e) {
                }

                if (!(!anElement1.getName().toLowerCase().equals("hcidentifier") || ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getValue()) > Integer.valueOf(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result2) > Integer.valueOf(0))))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return result1;


    }

    /**
     * Validation of instance by a constraint : ch_hpd_014_Metadata_attributes
     * Value of attributes HCRegulatedOrganization.HcSpecialisation and HCRegulatedOrganization.businessCategory must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]
     */
    private static boolean _validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_014_Metadata_attributes(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result3;
        result3 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("businesscategory")) {
                    result3.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }
        java.util.ArrayList<String> result2;
        result2 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        try {
            for (DsmlAttr anElement2 : result3) {
                result2.addAll(anElement2.getValue());
            }
        } catch (Exception e) {
        }
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement3 : result2) {
                String hcprofession2;
                hcprofession2 = anElement3.substring(Integer.valueOf(5) - 1, anElement3.length());

                String hcprofession3;
                hcprofession3 = hcprofession2.substring((hcprofession2.indexOf(":") + 1 + Integer.valueOf(1)) - 1, hcprofession2.length());


                if (!(aClass.matches(anElement3, "^BAG:[^:]*:[^:]*(:[^:]*)?") && ((!aClass.matches(anElement3, "^BAG:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.1.11", hcprofession3, hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null)) && (!aClass.matches(anElement3, "^BAG:[^:]*:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.1.11", hcprofession3.substring(Integer.valueOf(1) - 1, (hcprofession3.indexOf(":") + 1 - Integer.valueOf(1))), hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null))))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        java.util.ArrayList<DsmlAttr> result6;
        result6 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement4 : aClass.getAttr()) {
                if (anElement4.getName().toLowerCase().equals("hcspecialisation")) {
                    result6.add(anElement4);
                }
                // no else
            }
        } catch (Exception e) {
        }
        java.util.ArrayList<String> result5;
        result5 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        try {
            for (DsmlAttr anElement5 : result6) {
                result5.addAll(anElement5.getValue());
            }
        } catch (Exception e) {
        }
        Boolean result4;
        result4 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement6 : result5) {
                String hcprofession2;
                hcprofession2 = anElement6.substring(Integer.valueOf(5) - 1, anElement6.length());

                String hcprofession3;
                hcprofession3 = hcprofession2.substring((hcprofession2.indexOf(":") + 1 + Integer.valueOf(1)) - 1, hcprofession2.length());


                if (!(aClass.matches(anElement6, "^BAG:[^:]*:[^:]*(:[^:]*)?") && ((!aClass.matches(anElement6, "^BAG:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.1.18", hcprofession3, hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null)) && (!aClass.matches(anElement6, "^BAG:[^:]*:[^:]*:[^:]*") || net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesValueSet("2.16.756.5.30.1.127.3.10.1.18", hcprofession3.substring(Integer.valueOf(1) - 1, (hcprofession3.indexOf(":") + 1 - Integer.valueOf(1))), hcprofession2.substring(Integer.valueOf(1) - 1, (hcprofession2.indexOf(":") + 1 - Integer.valueOf(1))).toLowerCase(), null, null))))) {
                    result4 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (result1 && result4);


    }

    /**
     * Validation of instance by a constraint : ch_hpd_015_Organization_known_names
     * For  HPD Organizational provider  the parameter  Organization known names  (O) is required
     */
    private static boolean _validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_015_Organization_known_names(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result1;
        result1 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("o")) {
                    result1.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }

        return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > Integer.valueOf(0));


    }

    /**
     * Validation of template-constraint by a constraint : CHaddRequestForHCRegulatedOrganizationTemplate
     * Verify if an element can be token as a Template of type CHaddRequestForHCRegulatedOrganizationTemplate
     */
    private static boolean _isCHaddRequestForHCRegulatedOrganizationTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcregulatedorganization){1}.*");

    }

    /**
     * Validation of class-constraint : CHaddRequestForHCRegulatedOrganizationTemplate
     * Verify if an element of type CHaddRequestForHCRegulatedOrganizationTemplate can be validated by CHaddRequestForHCRegulatedOrganizationTemplate
     */
    public static boolean _isCHaddRequestForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return _isCHaddRequestForHCRegulatedOrganizationTemplateTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHaddRequestForHCRegulatedOrganizationTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     */
    public static void _validateCHaddRequestForHCRegulatedOrganizationTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
        if (_isCHaddRequestForHCRegulatedOrganizationTemplate(aClass)) {
            executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_004_Type(aClass, location, diagnostic);
            executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_013_IdentifierSyntax(aClass, location, diagnostic);
            executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_014_Metadata_attributes(aClass, location, diagnostic);
            executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_015_Organization_known_names(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_004_Type(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                   String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_004_Type(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_004_Type");
        notif.setDescription("For  HPD Organizational provider  the parameter  Org Type  (businessCategory) is required");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_004_Type");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-004"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_013_IdentifierSyntax(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                               String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_013_IdentifierSyntax(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_013_IdentifierSyntax");
        notif.setDescription("At least one value for hcidentifier must start with  RefData:OID: .");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_013_IdentifierSyntax");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-013"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_014_Metadata_attributes(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                                  String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_014_Metadata_attributes(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_014_Metadata_attributes");
        notif.setDescription("Value of attributes HCRegulatedOrganization.HcSpecialisation and HCRegulatedOrganization.businessCategory must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_014_Metadata_attributes");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-014"));
        diagnostic.add(notif);
    }

    private static void executeCons_CHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_015_Organization_known_names(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                                       String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCRegulatedOrganizationTemplate_Ch_hpd_015_Organization_known_names(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_015_Organization_known_names");
        notif.setDescription("For  HPD Organizational provider  the parameter  Organization known names  (O) is required");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_015_Organization_known_names");
        notif.getAssertions().add(new Assertion("CH-HPD", "CH-HPD-015"));
        diagnostic.add(notif);
    }

}
