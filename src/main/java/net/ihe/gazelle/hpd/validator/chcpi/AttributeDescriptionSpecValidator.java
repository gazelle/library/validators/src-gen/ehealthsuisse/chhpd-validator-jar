package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        AttributeDescriptionSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : AttributeDescription
 */
public final class AttributeDescriptionSpecValidator {


    private AttributeDescriptionSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_attr_attrdesc
     * The defined attributes for the CHCommunity ad CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview
     */
    private static boolean _validateAttributeDescriptionSpec_Constraint_ch_cpi_attr_attrdesc(net.ihe.gazelle.hpd.AttributeDescription aClass) {
        return (!(((String) aClass.getName()) == null) && (aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.2", aClass.getName().toLowerCase()) || aClass.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.3", aClass.getName().toLowerCase())));

    }

    /**
     * Validation of class-constraint : AttributeDescriptionSpec
     * Verify if an element of type AttributeDescriptionSpec can be validated by AttributeDescriptionSpec
     */
    public static boolean _isAttributeDescriptionSpec(net.ihe.gazelle.hpd.AttributeDescription aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   AttributeDescriptionSpec
     * class ::  net.ihe.gazelle.hpd.AttributeDescription
     */
    public static void _validateAttributeDescriptionSpec(net.ihe.gazelle.hpd.AttributeDescription aClass, String location, List<Notification> diagnostic) {
        if (_isAttributeDescriptionSpec(aClass)) {
            executeCons_AttributeDescriptionSpec_Constraint_ch_cpi_attr_attrdesc(aClass, location, diagnostic);
        }
    }

    private static void executeCons_AttributeDescriptionSpec_Constraint_ch_cpi_attr_attrdesc(net.ihe.gazelle.hpd.AttributeDescription aClass,
                                                                                             String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateAttributeDescriptionSpec_Constraint_ch_cpi_attr_attrdesc(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_attr_attrdesc");
        notif.setDescription("The defined attributes for the CHCommunity ad CHEndpoint object classes are listed in Appendix B. LDAP Schema Overview");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-AttributeDescriptionSpec-constraint_ch_cpi_attr_attrdesc");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-003"));
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-005"));
        diagnostic.add(notif);
    }

}
