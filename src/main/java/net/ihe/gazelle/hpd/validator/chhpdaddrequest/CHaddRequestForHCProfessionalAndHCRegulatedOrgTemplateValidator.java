package net.ihe.gazelle.hpd.validator.chhpdaddrequest;

import net.ihe.gazelle.hpd.DsmlAttr;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
 * package :   chhpdAddRequest
 * Template Class
 * Template identifier :
 * Class of test : AddRequest
 */
public final class CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateValidator {


    private CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateValidator() {
    }


    /**
     * Validation of instance by a constraint : ch_hpd_016_uid_format
     * The uid SHALL only contain one colon (:). Only the following characters are allowed in the DN without escaping: Alphanumeric, Minus “-“, Colon “:”, Exclamation mark “!”, Pipe symbol “|”, Underscore  _  and Full Stop  .
     */
    private static boolean _validateCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate_Ch_hpd_016_uid_format(net.ihe.gazelle.hpd.AddRequest aClass) {
        java.util.ArrayList<DsmlAttr> result3;
        result3 = new java.util.ArrayList<DsmlAttr>();

        /* Iterator Select: Select all elements which fulfill the condition. */
        try {
            for (DsmlAttr anElement1 : aClass.getAttr()) {
                if (anElement1.getName().toLowerCase().equals("uid")) {
                    result3.add(anElement1);
                }
                // no else
            }
        } catch (Exception e) {
        }
        java.util.ArrayList<String> result2;
        result2 = new java.util.ArrayList<String>();

        /* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
        try {
            for (DsmlAttr anElement2 : result3) {
                result2.addAll(anElement2.getValue());
            }
        } catch (Exception e) {
        }
        Boolean result1;
        result1 = true;

        /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
        try {
            for (String anElement3 : result2) {
                if (!(aClass.matches(anElement3, "^[^:]*:[^:]*$") && aClass.matches(anElement3, "^(\\\\[^\\p{Alnum}\\-:+!|]|[\\p{Alnum}\\-:+!|_\\.])*$"))) {
                    result1 = false;
                    break;
                }
                // no else
            }
        } catch (Exception e) {
        }

        return ((result1 && aClass.matches(((String) aClass.getDn()), "^.*(uid=[^:]*:[^:,]*),.*$")) && aClass.matches(((String) aClass.getDn()), "^.*(uid=(\\\\[^\\p{Alnum}\\-:+!|_\\.]|[\\p{Alnum}\\-:+!|_\\.])*),.*$"));


    }

    /**
     * Validation of template-constraint by a constraint : CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
     * Verify if an element can be token as a Template of type CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
     */
    private static boolean _isCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return aClass.matches(((String) aClass.getDn()).toLowerCase(), ".*(ou=hcprofessional|ou=hcregulatedorganization){1}.*");

    }

    /**
     * Validation of class-constraint : CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
     * Verify if an element of type CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate can be validated by CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
     */
    public static boolean _isCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate(net.ihe.gazelle.hpd.AddRequest aClass) {
        return _isCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateTemplate(aClass);
    }

    /**
     * Validate as a template
     * name ::   CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate
     * class ::  net.ihe.gazelle.hpd.AddRequest
     */
    public static void _validateCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate(net.ihe.gazelle.hpd.AddRequest aClass, String location, List<Notification> diagnostic) {
        if (_isCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate(aClass)) {
            executeCons_CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate_Ch_hpd_016_uid_format(aClass, location, diagnostic);
        }
    }

    private static void executeCons_CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate_Ch_hpd_016_uid_format(net.ihe.gazelle.hpd.AddRequest aClass,
                                                                                                                 String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateCHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate_Ch_hpd_016_uid_format(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("ch_hpd_016_uid_format");
        notif.setDescription("The uid SHALL only contain one colon (:). Only the following characters are allowed in the DN without escaping: Alphanumeric, Minus “-“, Colon “:”, Exclamation mark “!”, Pipe symbol “|”, Underscore  _  and Full Stop  .");
        notif.setLocation(location);
        notif.setIdentifiant("chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format");

        diagnostic.add(notif);
    }

}
