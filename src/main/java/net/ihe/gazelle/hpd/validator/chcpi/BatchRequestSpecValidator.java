package net.ihe.gazelle.hpd.validator.chcpi;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;


/**
 * class :        BatchRequestSpec
 * package :   chcpi
 * Constraint Spec Class
 * class of test : BatchRequest
 */
public final class BatchRequestSpecValidator {


    private BatchRequestSpecValidator() {
    }


    /**
     * Validation of instance by a constraint : constraint_ch_cpi_001
     * The only DSML element supported is the “searchRequest” element
     */
    private static boolean _validateBatchRequestSpec_Constraint_ch_cpi_001(net.ihe.gazelle.hpd.BatchRequest aClass) {
        return ((((((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAbandonRequest())).equals(Integer.valueOf(0)) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddRequest())).equals(Integer.valueOf(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getCompareRequest())).equals(Integer.valueOf(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getDelRequest())).equals(Integer.valueOf(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getExtendedRequest())).equals(Integer.valueOf(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModDNRequest())).equals(Integer.valueOf(0))) && ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getModifyRequest())).equals(Integer.valueOf(0)));

    }

    /**
     * Validation of class-constraint : BatchRequestSpec
     * Verify if an element of type BatchRequestSpec can be validated by BatchRequestSpec
     */
    public static boolean _isBatchRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass) {
        return true;
    }

    /**
     * Validate as a template
     * name ::   BatchRequestSpec
     * class ::  net.ihe.gazelle.hpd.BatchRequest
     */
    public static void _validateBatchRequestSpec(net.ihe.gazelle.hpd.BatchRequest aClass, String location, List<Notification> diagnostic) {
        if (_isBatchRequestSpec(aClass)) {
            executeCons_BatchRequestSpec_Constraint_ch_cpi_001(aClass, location, diagnostic);
        }
    }

    private static void executeCons_BatchRequestSpec_Constraint_ch_cpi_001(net.ihe.gazelle.hpd.BatchRequest aClass,
                                                                           String location, List<Notification> diagnostic) {
        Notification notif = null;
        try {
            if (!(_validateBatchRequestSpec_Constraint_ch_cpi_001(aClass))) {
                notif = new Error();
            } else {
                notif = new Note();
            }
        } catch (Exception e) {
            notif = new Error();
        }
        notif.setTest("constraint_ch_cpi_001");
        notif.setDescription("The only DSML element supported is the “searchRequest” element");
        notif.setLocation(location);
        notif.setIdentifiant("chcpi-BatchRequestSpec-constraint_ch_cpi_001");
        notif.getAssertions().add(new Assertion("CH-CPI", "CH-CPI-001"));
        diagnostic.add(notif);
    }

}
