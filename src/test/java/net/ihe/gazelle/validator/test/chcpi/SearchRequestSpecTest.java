package net.ihe.gazelle.validator.test.chcpi;

import org.junit.Assert;
import org.junit.Test;

public class SearchRequestSpecTest {

    CPITestUtil testExecutor = new CPITestUtil();

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. KO.
    public void test_ko_constraint_cpi_dnValid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.Dn.KO.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_002"));
    }

    /*@Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. KO.
    public void test_ko_constraint_cpi_dnValid2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.Dn2.KO.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_002"));
    }*/

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_dnValid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_002"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_shortDnValid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.ShortDN.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_002"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_chgatewayDN() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.dnCHGateway.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_002"));
    }

     @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_basedn() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.dnCHGateway.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_004"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ko_constraint_cpi_basedn() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.Dn2.KO.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_004", "Warning"));
    }




    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_chcommunity_without_dn_reference() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_006"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ok_constraint_cpi_chcommunity_dn_reference() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.DNReference.OK.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_006"));
    }

    @Test
    // The name attribut must be equals to one of the value in the value set 1.3.6.1.4.1.12559.11.25.1.15. OK.
    public void test_ko_constraint_cpi_chcommunity_dn_reference() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.DNReference.KO.xml",
                        "chcpi-SearchRequestSpec-constraint_ch_cpi_006"));
    }

}		
