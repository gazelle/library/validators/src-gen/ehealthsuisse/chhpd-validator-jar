package net.ihe.gazelle.validator.test.chhpd;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplateValidatorTest {

    AddRequestTestUtil testExecutor = new AddRequestTestUtil();

    @Test
    public void test_ok_constraint_hpdaddrequest_individual_uid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/HPDIndividual/Provider.OK.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ok_constraint_hpdaddrequest_org_uid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.OK.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ok_constraint_hpdaddrequest_org_uid2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uid.OK.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uid.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uid2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uid2.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uid3() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uid3.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ok_constraint_hpdaddrequest_org_uidindn() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uidindn.OK.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ok_constraint_hpdaddrequest_org_uidindn2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uidindn2.OK.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uidindn() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uidindn.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uidindn2() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uidindn2.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

    @Test
    public void test_ko_constraint_hpdaddrequest_org_uidindn3() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/HPDOrganizational/Org.Uidindn3.KO.xml",
                        "chhpdAddRequest-CHaddRequestForHCProfessionalAndHCRegulatedOrgTemplate-ch_hpd_016_uid_format"));
    }

}
