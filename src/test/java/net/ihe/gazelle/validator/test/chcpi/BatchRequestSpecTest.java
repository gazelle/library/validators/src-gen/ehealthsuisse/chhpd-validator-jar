package net.ihe.gazelle.validator.test.chcpi;

import org.junit.Assert;
import org.junit.Test;

public class BatchRequestSpecTest {

    CPITestUtil testExecutor = new CPITestUtil();

    @Test
    // The only DSML element supported is the “searchRequest” element. KO.
    public void test_ko_constraint_cpi_batchRequest() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.BatchRequest.KO.xml",
                        "chcpi-BatchRequestSpec-constraint_ch_cpi_001"));
    }

    @Test
    // The only DSML element supported is the “searchRequest” element. OK.
    public void test_ok_constraint_cpi_batchRequest() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/CPI/CPI.OK.xml",
                        "chcpi-BatchRequestSpec-constraint_ch_cpi_001"));
    }

}		
