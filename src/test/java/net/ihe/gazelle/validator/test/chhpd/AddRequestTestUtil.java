package net.ihe.gazelle.validator.test.chhpd;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hpd.BatchRequest;
import net.ihe.gazelle.hpd.validator.chhpdaddrequest.CHHPDADDREQUESTPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class AddRequestTestUtil extends AbstractValidator<BatchRequest> {

    static {
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return "https://ehealthsuisse.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
            }
        });
    }

    @Override
    protected void validate(BatchRequest message,
                            List<Notification> notifications) {
        BatchRequest.validateByModule(message, "/batchRequest", new CHHPDADDREQUESTPackValidator(), notifications);
    }

    @Override
    protected Class<BatchRequest> getMessageClass() {
        return BatchRequest.class;
    }
}

