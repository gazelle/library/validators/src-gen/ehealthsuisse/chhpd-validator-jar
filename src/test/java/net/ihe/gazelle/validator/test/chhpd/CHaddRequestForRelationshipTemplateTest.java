package net.ihe.gazelle.validator.test.chhpd;

import org.junit.Assert;
import org.junit.Test;

public class CHaddRequestForRelationshipTemplateTest {
    AddRequestTestUtil testExecutor = new AddRequestTestUtil();

    @Test
    // When the addRequest describes a relationship, the owner attribute is singled-valued and required. KO.
    public void test_ko_constraint_hpdaddrequest_Owner() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/ehealthsuisse/Relationship/Owner.KO.xml",
                        "chhpdAddRequest-CHaddRequestForRelationshipTemplate-ch_hpd_012_owner"));
    }

    @Test
    // When the addRequest describes a relationship, the owner attribute is singled-valued and required. OK.
    public void test_ok_constraint_hpdaddrequest_Owner() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/ehealthsuisse/Relationship/Owner.OK.xml",
                        "chhpdAddRequest-CHaddRequestForRelationshipTemplate-ch_hpd_012_owner"));
    }
}
