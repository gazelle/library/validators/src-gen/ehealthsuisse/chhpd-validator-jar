package net.ihe.gazelle.validator.test.chhpd;

import org.junit.Assert;
import org.junit.Test;
public class CHaddRequestForHCRegulatedOrganizationTemplateTest {

	AddRequestTestUtil testExecutor = new AddRequestTestUtil();

	@Test
	// Org Type (hcIdentifier) is required. KO.
	public void test_ko_constraint_hpdaddrequest_Type() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/OrgType.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_004_Type"));
	}

	@Test
	// Org Type (hcIdentifier) is required. OK.
	public void test_ok_constraint_hpdaddrequest_Type() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_004_Type"));
	}

	@Test
	// At least one value for hcidentifier must start with "RefData:OID:". KO.
	public void test_ko_constraint_hpdaddrequest_IdentifierSyntax() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/OrgIdentifiers.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_013_IdentifierSyntax"));
	}

	@Test
	// At least one value for hcidentifier must start with "RefData:OID:". OK.
	public void test_ok_constraint_hpdaddrequest_IdentifierSyntax() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_013_IdentifierSyntax"));
	}

	@Test
	// Value of attributes HCRegulatedOrganization.HcSpecialisation and HCRegulatedOrganization.businessCategory must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]
	public void test_ko_constraint_hpdaddrequest_Metadata() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.Metadata.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCRegulatedOrganization.HcSpecialisation and HCRegulatedOrganization.businessCategory must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]
	public void test_ko_constraint_hpdaddrequest_Metadata2() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.Metadata2.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>] OK.
	public void test_ok_constraint_hpdaddrequest_Metadata2() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.Metadata.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]. OK.
	public void test_ok_constraint_hpdaddrequest_Metadata() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_014_Metadata_attributes"));
	}

	@Test
	// Value of attributes HCRegulatedOrganization.HcSpecialisation and HCRegulatedOrganization.businessCategory must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>]
	public void test_ko_constraint_hpdaddrequest_Known_names() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.KnownNames.KO.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_015_Organization_known_names"));
	}

	@Test
	// Value of attributes HCProfessional.HcProfession and HCProfessional.HcSpecialisation must be provided in the following format : BAG:<CodeSystem>:<Code>[:<DisplayName>] OK.
	public void test_ok_constraint_hpdaddrequest_Known_names() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/ehealthsuisse/HPDOrganizational/Org.OK.xml",
						"chhpdAddRequest-CHaddRequestForHCRegulatedOrganizationTemplate-ch_hpd_015_Organization_known_names"));
	}
}		
